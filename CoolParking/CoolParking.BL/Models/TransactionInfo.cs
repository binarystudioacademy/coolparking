﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public readonly struct TransactionInfo
    {
        public DateTime Time { get; }
        public string Id { get; }
        public decimal Sum { get; }

        public TransactionInfo(string id, decimal sum)
        {
            Time = DateTime.Now;
            Id = id;
            Sum = sum;
        }
    }
}
