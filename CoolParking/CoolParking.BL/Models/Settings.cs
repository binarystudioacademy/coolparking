﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialBalance = 0;

        public const int Capacity = 10;

        public const int WithdrawingPeriod = 5000;

        public const int LoggingPeriod = 60000;

        public const decimal PassengerCarTariff = 2.0M;

        public const decimal TruckTariff = 5.0M;

        public const decimal BusTariff = 3.5M;

        public const decimal MotorcycleTariff = 1.0M;

        public const decimal FineCoefficient = 2.5M;
    }
}
