﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.UI
{
    class Program
    {
        private readonly static string[] _functions = new[]
        {
            "Current parking balance",
            "Session balance",
            "Number of free parking places",
            "Session transactions",
            "All transactions",
            "All parked vehicles",
            "Park vehicle",
            "Pick up vehicle",
            "Top up vehicle"
        };

        private static TimerService _withdrawTimer;
        private static TimerService _logTimer;
        private static ILogService _logService;
        private static ParkingService _parkingService;
        private static readonly string _logFilePath = "Transactions.log";       

        static void Main()
        {
            Initialize();

            DisplayTitle();

            ShowHeader();

            while (true)
            {                
                Console.WriteLine("Enter number of function or command: ");
                var pressedKey = Console.ReadLine();
                Console.WriteLine();

                if (pressedKey == "exit")
                {
                    break;
                };
                SelectFunction(pressedKey);
            }

            Dispose();
        }

        private static void Initialize()
        {
            _withdrawTimer = new TimerService()
            {
                Interval = Settings.WithdrawingPeriod
            };
            _logTimer = new TimerService()
            {
                Interval = Settings.LoggingPeriod
            };

            _logService = new LogService(_logFilePath);
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        private static void DisplayTitle()
        {
            Console.WriteLine("Welcome to Cool Parking");
        }

        private static void DisplayArray(Array array)
        {
            string shift = new string(' ', 3);
            for (int i = 0, j = 1; i < array.Length; i++, j++)
            {
                Console.WriteLine($"{shift}{j} - {array.GetValue(i)}");
            }
            Console.WriteLine();
        }

        private static void DisplayAvailableCommands()
        {
            Console.WriteLine(" * \"help\" to get the available commands");
            Console.WriteLine(" * \"menu\" to show all functions)");
            Console.WriteLine(" * \"exit\" to close the service");
            Console.WriteLine();
        }

        private static void SelectFunction(string input)
        {
            switch (input)
            {
                case "1":
                    ShowCurrentParkingBalance();                   
                    break;
                case "2":
                    ShowSessionBalance();                   
                    break;
                case "3":
                    ShowNumberOfFreeParkingPlaces();                    
                    break;
                case "4":
                    ShowSessionTransactions();                   
                    break;
                case "5":
                    ShowAllTransactions();                   
                    break;
                case "6":
                    ShowAllParkedVehicles();                   
                    break;
                case "7":
                    ParkVehicle();                   
                    break;
                case "8":
                    PickUpVehicle();
                    break;
                case "9":
                    TopUpVehicle();
                    break;
                case "help":
                    DisplayAvailableCommands();
                    break;
                case "menu":
                    DisplayArray(_functions);
                    break;
                default:
                    ShowNotRecognizedException();
                    break;

            }
        }

        private static void ShowNotRecognizedException()
        {
            Console.WriteLine("!!! Such a number of function or command is not recognized. Please enter again.\n");
        }

        private static void ShowCurrentParkingBalance()
        {
            Console.WriteLine($"Balance: {Parking.Instance.Balance}\n");
           
        }

        private static void ShowSessionBalance()
        {
            Console.Write($"Session balance: {_parkingService.GetLastParkingTransactions().Sum(i => i.Sum)}\n");
            Console.WriteLine();
        }

        private static void ShowNumberOfFreeParkingPlaces()
        {
            Console.WriteLine($"Free places: {_parkingService.GetFreePlaces()}\n");
        }

        private static void ShowSessionTransactions()
        {
            foreach (var transaction in _parkingService.GetLastParkingTransactions())
            {
                Console.WriteLine($"Time: {transaction.Time} ID: {transaction.Id} Sum: {transaction.Sum}");
            }
            Console.WriteLine();
        }

        private static void ShowAllTransactions()
        {
            Console.WriteLine(_logService.Read());
        }

        private static void ShowAllParkedVehicles()
        {
            foreach (var vehicle in Parking.Instance.Vehicles)
            {
                Console.WriteLine($"ID: {vehicle.Id} Type: {vehicle.VehicleType} Balance: {vehicle.Balance}");
            }
            Console.WriteLine();
        }

        private static void ParkVehicle()
        {
            Console.WriteLine("Enter information about transport:");

            Console.Write("ID (format: \"XX-YYYY-XX\", where X is any uppercase letter and Y is any digit): ");
            string id = Console.ReadLine();

            DisplayVehicleTypes();
            Console.Write("Vehicle type (enter number that indicates type of vehicle suggested): ");
            Enum.TryParse(typeof(VehicleType), Console.ReadLine(), out object vehicleType);

            Console.Write("Balance: ");
            decimal.TryParse(Console.ReadLine(), out decimal balance);

            try
            {
                _parkingService.AddVehicle(new Vehicle(id, (VehicleType)vehicleType - 1, balance));
                Console.WriteLine("\nThe vehicle was successfully added!\n");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("\nInvalid data entered. Please try again\n");
            }
        }

        private static void DisplayVehicleTypes()
        {
            VehicleType[] vehicleTypes = (VehicleType[])Enum.GetValues(typeof(VehicleType));
            Console.WriteLine();
            DisplayArray(vehicleTypes);
        }

        private static void PickUpVehicle()
        {
            Console.Write("ID (format: \"XX-YYYY-XX\", where X is any uppercase letter and Y is any digit): ");
            string id = Console.ReadLine();

            try
            {
                _parkingService.RemoveVehicle(id);
                Console.WriteLine("\nThe vehicle was successfully removed!\n");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine($"\n{e.Message}\n");
            }
        }

        private static void TopUpVehicle()
        {
            Console.Write("ID (format: \"XX-YYYY-XX\", where X is any uppercase letter and Y is any digit): ");
            string id = Console.ReadLine();

            Console.Write("\nAmount: ");
            decimal.TryParse(Console.ReadLine(), out decimal sum);

            try
            {
                _parkingService.TopUpVehicle(id, sum);
                Console.WriteLine("\nThe vehicle balance was successfully top upped!\n");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine($"\n{e.Message}\n");
            }
        }

        private static void Dispose()
        {
            _withdrawTimer.Stop();
            _logTimer.Stop();

            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parkingService.Dispose();
        } 
        private static void ShowHeader()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(" All functoions:\n");
            DisplayArray(_functions);
            DisplayAvailableCommands();
        }
    }
}